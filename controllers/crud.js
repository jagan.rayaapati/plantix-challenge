const fs = require("fs");
const constants = require("../constants.js");

function getAll() {
  const studentsData = fs.readFileSync(constants.jsonPath);
  return studentsData.toString();
}

function getByRollNumber(query) {
  const studentsData = fs.readFileSync(constants.jsonPath);
  let arrayOfStudents = JSON.parse(studentsData.toString());
  let studentRecord;
  for (let student of arrayOfStudents) {
    if (student.rollNumber == query.number) {
      studentRecord = student;
    }
  }
  return studentRecord;
}

function create(body) {
  const studentsData = fs.readFileSync(constants.jsonPath);
  let arrayOfStudents = JSON.parse(studentsData.toString());
  let newStudent = body;
  let studentRecord;
  if (Object.keys(newStudent).length == 4) {
    for (let student of arrayOfStudents) {
      if (student.rollNumber == body.rollNumber) {
        studentRecord = false;
      }
    }
    if (studentRecord == undefined) {
        studentRecord = true;
      arrayOfStudents.push(newStudent);
      fs.writeFileSync(constants.jsonPath, JSON.stringify(arrayOfStudents));
    }
  }
  return studentRecord;
}

function update(body) {
  const studentsData = fs.readFileSync(constants.jsonPath);
  let arrayOfStudents = JSON.parse(studentsData.toString());
  let updateStudent = body;
  let studentRecord;
  if (Object.keys(updateStudent).length == 4) {
    for (let studentIndex in arrayOfStudents) {
      let student = arrayOfStudents[studentIndex];
      if (student.rollNumber == body.rollNumber) {
        arrayOfStudents[studentIndex] = updateStudent;
        studentRecord = updateStudent;
        fs.writeFileSync(constants.jsonPath, JSON.stringify(arrayOfStudents));
      }
    }
    return studentRecord;
  }
}

function deleteRecord(query) {
  const studentsData = fs.readFileSync(constants.jsonPath);
  let arrayOfStudents = JSON.parse(studentsData.toString());
  let studentRecord;
  for (let studentIndex in arrayOfStudents) {
    let student = arrayOfStudents[studentIndex];
    if (student.rollNumber == query.number) {
      arrayOfStudents.splice(studentIndex, 1);
      studentRecord = true;
      fs.writeFileSync(constants.jsonPath, JSON.stringify(arrayOfStudents));
    }
  }
  return studentRecord;
}

module.exports = {
  getAll,
  getByRollNumber,
  create,
  update,
  deleteRecord,
};
