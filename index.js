const express = require("express");
const { type } = require("os");
const app = express();
const port = 3000;

const {
  getAll,
  getByRollNumber,
  create,
  update,
  deleteRecord,
} = require("./controllers/crud.js");

app.use(express.json()); // for parsing application/json

/** The home route/path gives the entire list of students */
app.get("/", (req, res) => {
  res.send(getAll());
});

/** If the roll number is specified, only that record is returned */
app.get("/getByRollNumber", (req, res) => {
  let studentRecord = getByRollNumber(req.query);
  if (studentRecord) {
    res.send(studentRecord);
  } else if (req.query.number) {
    res.status(404).send("Record not found");
  } else {
    res.status(400).send("Bad request. Student roll number missing.");
  }
});

/** A route to insert a new record */
app.post("/create", (req, res) => {
  let newStudent = req.body;
  let studentRecord = create(newStudent);
  if (Object.keys(newStudent).length == 4) {
    if (studentRecord == true) {
      res.send("Created successfully");
    } else if (studentRecord == false) {
      res.send("Roll number already present. Please use update url");
    }
  } else {
    res
      .status(400)
      .send("Bad request. Record to insert not present/incorrect.");
  }
});

/** A route to update an existing record */
app.patch("/update", (req, res) => {
  let updateStudent = req.body;
  let studentRecord = update(updateStudent);
  if (Object.keys(updateStudent).length == 4) {
    if (studentRecord) {
      res.send("Student data updated");
    }
    if (studentRecord == undefined) {
      res.send("Unable to update the student record. Please try to create");
    }
  } else {
    res.status(400).send("Bad request. Record to update is incorrect.");
  }
});

/** A route to delete a new record */
app.delete("/delete", (req, res) => {
  let studentRecord = deleteRecord(req.query);
  if (studentRecord) {
    res.send(`Student with roll number ${req.query.number} deleted`);
  } else if (req.query.number) {
    res.status(404).send("Record not found");
  } else {
    res.status(400).send("Bad request. Student roll number missing.");
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
