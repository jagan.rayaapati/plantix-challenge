const fs = require("fs");
const constants = require("../constants.js");
const {
  getAll,
  getByRollNumber,
  create,
  update,
  deleteRecord,
} = require("../controllers/crud.js");
let originalData;
let intialState;

beforeEach(() => {
  originalData = fs.readFileSync(constants.jsonPath).toString();
  intialState = [
    { name: "ABC", rollNumber: "1", mathematics: "70", physics: "70" },
    { name: "DEF", rollNumber: "2", mathematics: "45", physics: "45" },
  ];
  fs.writeFileSync(constants.jsonPath, JSON.stringify(intialState));
});

afterEach(() => {
    fs.writeFileSync(constants.jsonPath, originalData);
});

test("get all records", () => {
  expect(
    getAll()
  ).toEqual(JSON.stringify(intialState));
});

test("get student with roll number", ()=> {
  expect(
    JSON.stringify(getByRollNumber({number: "1"}))
  ).toEqual(JSON.stringify(intialState[0]))
})

test("add a student record", () => {
  expect(
    create({ name: "ABC", rollNumber: "5", mathematics: "70", physics: "45" })
  ).toBeTruthy();
});

test("update a student record", ()=>{
  expect(
    JSON.stringify(update({ name: "ABC", rollNumber: "1", mathematics: "100", physics: "100" }))
  ).toEqual(JSON.stringify({ name: "ABC", rollNumber: "1", mathematics: "100", physics: "100" }));
})

test("delete a student record", ()=>{
  expect(
    deleteRecord({ number: "1" })
  ).toBeTruthy();
})

