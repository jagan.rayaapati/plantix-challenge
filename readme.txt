Code challenge:
Write 4 CURD operation APIs without using any framework.
APIs will parse request inputs and validate it.
Store and fetch the data from a JSON File.
Each API should have at least 1 unit test.



* JSON file resides in `data/records.json`
* All CRUD operation definitions are found in `controllers/crud.js`
* Express server (http server) and paths are defined in `index.js`
* All constants like file path etc. reside in `constants.js`
* Unit tests are written using Jest and they reside in `tests` folder. Currently has one file `crud.test.js` with 5 tests and 1 test suite
* To start the application stay in project root and use the command `npm start` or `node index.js`
* To start the application for development using nodemon, run `npm run dev`
* To execute the unit tests run `npm test`